init python:
    class Classes:
        def __init__(self, short_name, long_name, availability, is_locked):
            self.short_name = short_name
            self.long_name = long_name
            self.availability = availability
            self.is_locked = is_locked

    class Activities:
        def __init__(self, short_name, long_name, availability, is_locked):
            self.short_name = short_name
            self.long_name = long_name
            self.availability = availability
            self.is_locked = is_locked
