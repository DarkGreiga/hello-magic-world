screen mc_maker:
    vbox:
        text "Customize Your Character" xalign 0.5 size 25
        grid 2 1:
            spacing 0
            xfill True
            frame:
                style "frame_style"
                add "#800"
                add DynamicDisplayable(draw_char,"aeolus") xalign 0.5
                hbox:
                    xalign 0.5
                    yalign 0.9
                    text "[name] [surname]" size 25
            frame:
                style "frame_style"
                add "#008"
                text "Hello world right" style "frame_style"
                if is_running:
                    textbutton "Save and Return" text_size 30 xalign 0.5 yalign 0.9 action Return()
                else:
                    textbutton "Start Game!" text_size 40 xalign 0.5 yalign 0.9 action Jump("game_introduction")

screen schedule_screen:
    vbox:
        text "Schedule Setup" xalign 0.5 size 25
        grid 1 2:
            spacing 0
            yfill True
            frame:
                style "frame_style"
                null height 100
                grid 8 5:
                    spacing 1
                    xalign 0.5
                    yalign 0.5

                    # loop to build the header
                    for i in range(8):
                        frame:
                            style "frame_style"
                            xsize 141
                            ysize 54
                            add "schedule_topheader.png"
                            text header_title[i] style "header_button_style"

                    # loop to build the time titles and blank cells
                    for i in range(32):
                        # different first cell
                        if i % 8 == 0:
                            frame:
                                style "frame_style"
                                xsize 141
                                ysize 54
                                add "schedule_sideheader.png"
                                text time_title[i/8] style "header_button_style"
                        else:
                            frame:
                                style "frame_style"
                                xsize 141
                                ysize 54
                                if cur_schedule[i-(i/8+1)] == None:
                                    add "schedule_cells.png"
                                    text "" style "header_button_style"
                                else:
                                    if isinstance(cur_schedule[i-(i/8+1)], Activities):
                                        add "activity_cells.png"
                                    elif isinstance(cur_schedule[i-(i/8+1)], Classes):
                                        add "class_cells.png"
                                    else:
                                        add "schedule_cells.png"
                                    text cur_schedule[i-(i/8+1)].long_name style "header_button_style"
                                button:
                                    style "schedule_cell"
                                    action [SetVariable("day_name", i%8), SetVariable("time_name", i/8)]

            frame:
                style "frame_style"
                hbox:
                    vbox:
                        style "class_activity"
                        vbox:
                            style "class_activity2"
                            text "Classes:" style "class_activity_text"
                        vbox:
                            style "class_activity"
                            if day_name == 0:
                                text "No classes available." style "class_activity_text2"
                            else:
                                if len(schedule_class_list[day_name][time_name]) == 0:
                                    text "No classes available." style "class_activity_text2"
                                else:
                                    grid 3 4:
                                        xalign 0.5
                                        spacing 2
                                        $ temp = schedule_class_list[day_name][time_name]
                                        for i in range(12):
                                            if i >= len(schedule_class_list[day_name][time_name]):
                                                null
                                            else:
                                                frame:
                                                    style "frame_style"
                                                    xsize 141
                                                    ysize 54
                                                    add "class_cells.png"
                                                    text class_list[temp[i]].long_name style "header_button_style"
                                                    button:
                                                        style "schedule_cell"
                                                        action Function(add_to_schedule,class_list[temp[i]])
                    vbox:
                        style "class_activity"
                        vbox:
                            style "class_activity2"
                            text "Activities:" style "class_activity_text"
                        vbox:
                            style "class_activity"
                            if day_name == 0:
                                text "No activities available." style "class_activity_text2"
                            else:
                                if len(schedule_activity_list[day_name][time_name]) == 0:
                                    text "No activities available." style "class_activity_text2"
                                else:
                                    grid 3 4:
                                        xalign 0.5
                                        spacing 2
                                        $ temp = schedule_activity_list[day_name][time_name]
                                        for i in range(12):
                                            if i >= len(schedule_activity_list[day_name][time_name]):
                                                null
                                            else:
                                                frame:
                                                    style "frame_style"
                                                    xsize 141
                                                    ysize 54
                                                    add "activity_cells.png"
                                                    text activity_list[temp[i]].long_name style "header_button_style"
                                                    button:
                                                        style "schedule_cell"
                                                        action Function(add_to_schedule,activity_list[temp[i]])
                    vbox:
                        style "bottom_right_btn"
                        textbutton "Clear schedule":
                            background "#800"
                            action Confirm("Are you sure you want to clear the current schedule setup?", Function(clear_schedule), NullAction())
                        textbutton "Done":
                            background "#800"
                            action Confirm("Is this schedule setup okay?",Jump("start"),NullAction())
