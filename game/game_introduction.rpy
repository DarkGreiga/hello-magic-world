define mc = Character("[name]")
define i = Character("Isa")
define m = Character("Mayari")

label game_introduction:

    $is_running = True
    
    $grade = 0

    scene bg apartment

#Panicked MC face
    mc "Oh, shit! I'm going to be late!"

#Neutral Isa face
    i "You're not going to be late. We still have a few hours left."

#Panicked MC face
    mc "I can't find anything, and I know something is going to go wrong and, and, I bet I forget something important and then I won't be let into the opening ceremony and-"

#More of a yelly MC face
    mc "Argh!"

#Lol Isa face
    i "Maybe if you stop running around the apartment like a monkey with its tail on fire, you'll be able to find what you're looking for."

#Confused MC face
    mc "What sort of saying is that?"

#Lol Isa face
    i "A good one if it got you to stop."

    "Today is my very first day attending the Challurim Academy for Adroit Development, a magical university. Only students who have discovered any magical ability by the age of 18 can attend."

    "I am one of those students. Less than a year ago, an old man came to my home and told me that I had magic. It was hard to believe since I had been a normal person up until then."

    "Even now, I still can't believe that I'm some sort of sorcerer."

#Neutral MC face
    mc "Okay, I stopped. But standing here won't help me find my... forms."

    "The enrolment forms I had so desperately been looking for were laying innocently on the couch that I had searched a thousand times over."

#-_- MC face
    mc "Don't say anything."

#Happy Isa face
    i "Hehe, wasn't going to."

#-_- MC face
    mc "Liar."

    "This is my best friend from childhood, Isamor. But I call them Isa for short."

    "Isa used to live next door to my family. When I realised how far I'd have to move to attend this university, Isa offered to come with me so I wouldn't be by myself in a new place."

#Thinky Isa face
    i "Do you really need all those forms with you?"

#Surprised MC face
    mc "I... {w=1.5}don't know."

#Thinky MC face
    mc "But it's better to be prepared. What if they want proof that I'm actually enroled there? I don't want to miss out on the entrance ceremony because I forgot some papers."

#Neutral Isa face
    i "You really are worried about this entrance ceremony, aren't you?"

#Panicked MC face
    mc "The letter said that we'll be tested at the ceremony, and I don't know any magic! What do you think?"

#Thinky Isa face
    i "Can't you wave your hands and say some fancy rhyme?"

#Serious MC face
    mc "That's not how magic works!"

#Explainy MC face
    mc "The man who offered me to study here even said, I might have magic but without study I can't do anything with it. It's like any skill. You don't just wake up one day decide you can now play the harp."

    if male_flag:

        #Meh Isa face
        i "If you say so. You're the wizard here."

    if female_flag:

        #Meh Isa face
        i "If you say so. You're the witch here."

#Neutral MC face
    mc "Anyway, I need to find the rest of my paperwork before we leave."

#Neutral Isa face
    i "Try your room."

#Confused MC face
    mc "My room. Right."

#Scene change to in front of school

    scene bg_outerstreet
    with dissolve

#Happy MC face
    mc "Thanks for coming with me, Isa."

#Happy Isa face
    i "It's nothing. I couldn't miss my friend's first day at magic school."

#Neutral MC face
    mc "Shh, don't say that in the open like that!"

#Surprised Isa face
    "Isa covered their mouth. Magic was meant to be a secret from the rest of the world, so neither of us were allowed to talk about it to anyone that wasn't magical nor living with someone magical."

#Calm MC face
    mc "I really am glad you're here, but I hope I'm not keeping you from work."

#Happy Isa face
    i "Don't worry, the art requests will be waiting to be filled when I get back anyway."

#Neutral MC face
    mc "You're so lucky you get to work full time on your art. You never have to worry about another test again, ever."

#Neutral Isa face
    i "Still worried about it?"

#Panicked MC face
    mc "Ugh, it's even worse now that I can see the school."

#Confused Isa face
    i "Speaking of the school, is this it? It looks so... plain."

#Neutral MC face
    mc "What were you expecting?"

#Thinky Isa face
    i "A castle, at least. With some dragons flying around it."

#Deadpan MC face
    mc "We're in the middle of a city."

#Thinky Isa face
    i "Okay, so no dragons."

#Deadpand MC face
    mc "Come on."

# Leave this scene black

    scene black
    with dissolve

    "We walked into the building, which brought us to a clean looking lobby. A man behind the reception desk took one look at my uniform and told us to head down the end of the corridor."

    "There was only one door in this corridor, though Isa looked around confused. I shrugged, heading forward, with Isa following me shortly. Once we made it to the door, I pushed it open to reveal the school."

# Scene of the school here

    scene bg_academy
    with dissolve

#Surprised MC face
    mc "Wow."

#Surprised Isa face
    i "How did they fit all this inside a building?"

#Surprised MC face
    mc "I don't know."

    "There it was, something that looked like a magic school. It was even placed in a meadow nowhere near the city we were just in seconds ago."

    "Different students with their guests walked around us, with a large majority headed towards a group of people yelling \"Opening ceremony, this way!\" near the entrance of the building. So we followed the others."

    "Student Rep" "First year?"

#Panicked MC face
    mc "Yes. {w=1}Ah! Do you need me to show any of my proof of enrolment?"

    "Student Rep" "Just your name will do for now."

#Deflated MC face
    mc "Oh... it's [name] [surname]."

    "The student looked down at a clipboard they held, running the end of a pen down the list of names until mine was found."

    "Student Rep" "There it is. [name], you'll be headed down this way with the other first years. Please be sure to listen to our guide who will direct you to the right place."

#Neutral MC face
    mc "Okay, thanks."

    "When the student was done pointing directions out at me, they then turned to Isa and pointed in the opposite direction."

    "Student Rep" "As for you, guests are asked to go over there where they will be directed to their seats."

#Neutral Isa face
    i "Alright."

#Happy Isa face
    i "Guess I'll be seeing you later on, [name]. Good luck and try not to stress to much."

#Happy MC face
    mc "No promises. See you soon."

    "With that, Isa and I went out separate ways. As I went down the direction the student pointed to me, I instantly ran into another student who took me to a large room where all the other first years were waiting."

    scene bg_classroom
    with dissolve

#Talon is skipping scenes she is struggling to think of#

    "A man walked in, though he didn't have a uniform like everyone else in the room. Instead, he wore a professional looking outfit. He must be one of the teachers here."

#Neutral Falorin face
    "Teacher" "Attention, everyone! The opening ceremony is about to begin."

    "He spoke in a clear and loud voice that rang above the chatter of newly enroled students. Everyone stopped in the midst of their conversations to focus on this teacher."

#Neutral Falorin face
    "Teacher" "Before we leave, I will explain how the opening ceremony is held in Chaloura Academy. We will take you to your seats in the auditorium, where the ceremony is held. One at a time, you will be called to come on stage for your course to be selected."

    "Teacher" "The academy has a tradition in relying on an outdated ritual to choose your courses for you. To perform the ritual, you will be given a goblet filled with a melange liquid. Then you will walk onto the pentagram drawn on the stage, but be careful not to step on any of the lines of the pentagram."

    "Teacher" "In the centre will be a basin that you will pour the contents of your goblet into. This will create an optical phenomenon and we will announce what courses you will be taking. Then you will leave the stage and return to your seat."

#Neutral Falorin face
    "Teacher" "Any questions?"

    "There was a long pause as everyone took in that information. Then, one student too far away for me to see raised their hand above the crowd."

    "Student" "What's an optical phenomena?"

#Neutral Falorin face
    "Teacher" "Events that can be observed only when particular conditions for a specific interaction of light and matter occur. Think a rainbow."

    "A student near my was speaking in a whisper just loud enough for me to hear."

    "Student" "I thought we were going to a magic school. This sounds like my old science classes in high school."

#Neutral Falroin face
    "Teacher" "If there are no more questions, please follow me in a single file line."

    "Everyone followed the teacher without another word as he led us to where the opening ceremony was being held."

#The scene for where the opening ceremony happens will  be written when Talon knows if an auditorium needs to be drawn or not

    scene bg_office
    with dissolve

    "I followed the Rector to their office. They haven't said anything, until we got to their office."

    "Rector" "While this situation is unusual, it isn't unheard of for the ritual to fail to select a course for someone."

#Confused MC face
    mc "So I didn't mess anything up?"

    "Rector" "No, the ritual behaved as it's supposed to."

    "Despite saying that, the Rector's face was still looking grave."

#Confused MC face
    mc "Then... what classes should I take?"

    "Rector" "That will depend on if you continue to study at this academy."

#Confused MC face
    mc "Excuse me...? Why wouldn't I want to be here?"

    "Rector" "The ritual at the opening ceremony is intended to test one's magical aptitude. That much was explained at the ceremony."

    "That's right. The ritual was meant to tell us what subjects our magic was best suited to so we could focus on that."

#Confused MC face
    mc "But the ritual didn't pick anything for me. What does that mean?"

    "Rector" "It means that your magical aptitude is especially low. Any other student who has been found in your situation often cannot keep up with their classes and end up failing all their exams."

#Shocked MC face
    mc "What? So I can't learn any magic?"

    "Rector" "You can, though with extreme difficulty. It would be easier for you if you were to withdraw your enrolement before the semester begins. Of course, we would fully refund your tuition."

#Sad MC face
    mc "But-but, I don't want to drop out."

    "Rector" "I understand how you feel. However, you must consider what is best for your well being."

#Sad MC face
    mc "What's the other options? The not dropping out options?"

    "The Rector sighed."

    "Rector" "If you do continue to study here, instead of putting you into any major and submjaor, you will be allowed to sit in on every class. This way, you can find what magic you may be able to develop some... ability in."

#Surprise MC face
    mc "Really? I could take all the classes?"

    "Rector" "That is an option. Though there's no guarantee you will be capable of casting the spells in every one of those classes."

#Neutral Mc face
    mc "Okay. I think I'll take that option."

    "Rector" "Don't be too hasty to come to a decision. If you do choose to stay, you must know that you are expected to maintain the minimum of a passing grade throughout the year. Any student who fails to uphold the minimum grade required will not be able to continue into their next year."

#Deflated MC face
    mc "A minimum grade requirement? Is it that hard to get a passing grade?"

    "Rector" "It usually is for students who have been in your situation. This is why I implore you to consider withdrawing."

#Neutral MC face
    mc "... {w=1.5}Do I have to make a choice now?"

    "Rector" "Of course not. You do have until the end of census date to withdraw and still get a refund on your tuition. Use that time to consider your options."

#Neutral MC face
    mc "Okay. I'd like to use that time to think about it."

    "Rector" "Very well. You can return any time before the census date to notify me of your decision."

#Neutral MC face
    mc "I will."

    "Rector" "You may leave now. I'm sure your family that came with you are waiting to find out what's happened."

#Confused MC face
    mc "Family? Ah... right."

    "It wasn't worth correcting the Rector on that point, so I left without a word. Besides, Isa probably was worried about me since I was rushed out as soon as the ceremony ended."

    scene bg_academy
    with dissolve

    "After some time of searching for Isa in the crowd, I found zir looking around the place for me."

#A MC face, I don't know
    mc "Isa!"

#Happy Isa face
    i "[name]! There you are!"

#Worried Isa face
    i "Are you okay? What happened?"

#Done MC face
    mc "Too much happened... let's go home and talk about it. I don't really feel up to taking a tour of the place with everyone."

    "Isa doesn't try to ask anymore questions after that. We both then made our way home, where I explained what hapepned in the Rector's office."

    scene bg apartment
    with dissolve

    i "They want you to drop out?"

    mc "Yes. The Rector seems to think I won't be able to learn enough magic to pass the year."

    i "Are you going to?"

    mc "No! {w=1}I don't know. I don't want to, but if the exams are too hard, I might be wasting my time."

    i "There's no harm in trying some of the classes and finding out how hard they are, right?"

    mc "I know that. It's just... this all got so complicated."

    if male_flag:

         #Frustrated MC face
         mc "All I wanted was become a wizard! Why does it have to be so hard?"

    if female_flag:

        #Frustrated MC face
         mc "All I wanted was become a witch! Why does it have to be so hard?"

#Sweatdrop Isa face
    i "I don't know what to say. I don't know anything about magic so I can't give you any advice."

#Encouraging Isa face
    i "But since you have time to think about, you can at least go to the first week of classes and work out if it really is that difficult."

#Relieved MC face
    mc "You're right. I can at least check out the first week. Thanks, Isa."

#Sweatdrop Isa face
    i "I wouldn't thank me yet. You still have to work out what to do after you've taken some classes."

#Neutral MC face
    mc "Yeah... I hope they won't be too hard."

#Thinky MC face
    mc "Since I have the weekly time table for all classes, I should plan out what I'll be doing for this week."

#
#INSERT THE SCHEDULE PLANNER THINGO MAGINO
#

#Neutral Isa face
    i "Do you feel better now that you've organised your week?"

#Neutral MC face
    mc "A little. But I'll be better once this week is over."

#Neutral Isa face
    i "Maybe you should sleep early for tomorrow. I know after a rough day all I want to do is fall asleep."

#Tired MC face
    mc "Thanks Isa. That sounds like a good idea. I'll turn in."

#Happy Isa face
    i "Goodnight, [name]."

#
# SLEEPY TIMES
#

    scene black
    with dissolve

    scene bg_apartment
    with dissolve

#Tired MC face
    mc "Morning, Isa."

#Shocked Isa face
    i "Oh, goodmorning. How was your sleep?"

    "Isa was currently busy sorting out different paints that were sprawled on the ground. Ze had jumped up in surprise when responding, only having now noticed me entering the room."

#Neutral MC face
    mc "Alright. Got an art request to do?"

#Neutral Isa face
    i "Mhmm. It came through last night, so I'm starting it today."

#Nervous Isa face
    i "Oh, but I don't need to start on it now. So I can walk you to school if you want."

#Surprised MC face
    mc "Oh, no. You don't have to do that. I'm fine to go alone, and I don't want to interrupt you with your work."

#Neutral Isa face
    i "You wouldn't be interrupting-"

#Nervous MC face
    mc "Really, I'll be fine to go alone. I'm feeling a lot better after sleeping."

    "I wasn't, but I couldn't let Isa drop hir work only because I was worried about what happened yesterday."

#Confused Isa face
    i "If you're sure..."

#Happy MC face
    mc "I am. So focus on your work."

#Neutral Isa face
    i "Alright, I will. Good luck on your first day of magic school."

#Happy MC face
    mc "Thanks. And good luck to you too, on your art."

#
# GO TO SCHOOL
#

    scene bg_auditorium
    with dissolve

    "My first day at magic school started back at the auditorium where the opening ceremony yesterday had been held. Instead of going to class, the student body sat at an assembly, as the Rector was getting up on the stage."

    "Rector" "Welcome to your first day at Challurim Academy for Adroit Development. It is within these halls where many upstanding mages in society have taken their first steps into becoming true mages. From today, you have the great opportunity to follow the paths of experts such as Ralner Garueal, Syrie Louvra, and Dibeyn Cobait."

    "I have absolutely no idea who any of those people are. I hope this isn't going to be on a test."

    "Rector" "You've come here, naive of the world of magic, waiting to be filled with the knowledge obtained by those before you. Diligence and conscientious effort are all necessary in order to hone your magic, however, that alone is not always sufficient."

    "Rector" "In the academy, we believe in giving proper guidance to the new is the key to success. Through guidance, knowledge and traditions can be kept alive among the younger generation."

    "Rector" "Through the course of Challurim Academy's traditions, the students of the finishing class will learn the importance of providing guidance by becoming mentors to those of the new class."

    "Rector" "This is a grand opportunity for both students to learn important values in working together as a community. Of course, the knowledge our seniors can impart on our freshmen will be highly valued, but our seniors can also learn something from their freshmen as well."

    "Rector" "Today, I will assign seniors to a newly enroled student to mentor. Please come to the front as your names are called."

    "Soon, the Rector was calling student after student. In most situations, it was one senior and one freshmen being called, but sometimes there would be two freshmen instead. I suppose there were more freshmen than seniors."

    "Eventually, my name was called, and I came face to face with a girl much shorter than me."

    "Rector" "And that concludes our assembly today. Mentors, take this time to show your student around the academy."

    "As soon as the Rector left the stage, the auditorium erupted into chatter."

#Neutral MC face
    mc "Um, so it was Mayari?"

#Happy Mayari face
    m "Oh em gee! Are you that student who broke the opening ceremony spell?"

#Nervous MC face
    mc "Yeah... but I rather you call me [name]. It's a lot easier to say than all that..."

#Happyy Mayari face
    m "Kay, kay. I'm so lucky to be paired up with you. Not many mages here can say they mentored a super mage."

#Confused MC face
    mc "Super mage?"

#Neutral Mayari face
    m "Yeah, everyone's been talking about it. Like your magic is so super you can learn all the spells."

#Nervous MC face
    mc "I wouldn't say I can learn all the spells..."

#Happy Mayari face
    m "Don't be so modest. Now come on, come on. Let me show you around Chaad."

#Confused MC face
    mc "Chaad?"

#Neutral Mayari face
    m "That's the initials of this school's name."

#Confused MC face
    mc "But where do you get a H? Isn't it C, A, A, and D?"

#Neutral Mayari face
    m "Well, it's in Challurium. And if you didn't have the H, the the school would just be called Caad, and no one wants to be a Caad."

#Confused MC face
    mc "And being a Chaad is better?"

#Happy Mayari face
    m "Soooo much better. Trust the internet on it."

#Lost MC face
    mc "..."

#Neutral Mayari face
    m "Now let's go."

    "Mayari skips out of the auditorium before I can even respond. With no other choice, I follow her."

    scene bg_mainbuilding
    with dissolve

#Neutral Mayari face
    m "Here's the main building for the academy. All the classes are held here."

#Neutral MC face
    mc "Are you going to show me where the classrooms are?"

#Neutral Mayari face
    m "Oh no, there's no need for that. There's only two classes you're in, so it'll be easy enough for you to find."

#Worried MC face
    mc "But I have six classes to take..."

#Surprise Mayari face
    m "Oh? You have six?"

#Smiley Mayari face
    m "Weeeell, I only took two classes, so I wouldn't know where all the classrooms are to show you. But you must be super smart if they're letting you take ALL of them. So, you'll be fine!"

#Nerous MC face
    mc "I'm not really that smart..."

#Smiley Mayari face
    m "El oh el, you're just so modest. You need to stop, it's too cute."

#Derp MC face
    mc "Uh... but I'm not... either of those..."

#Happy Mayari face
    m "Come on, we still have the rest of the tour. There's more important things than classrooms to see!"

    "Again, Mayari was walking off ahead. And again, I was left with no choice but to follow her."

    scene bg_cafeteria
    with dissolve

#Neutral Mayari
    m "Here's the place where we get our food needs filled, the cafeteria. Better make sure you remember where this is, it's very important."

#Smiley MC
    mc "More important than knowing where my classrooms are?"

#Determined Mayari
    m "Totally! The professors here don't care if you miss a class but food... food is life!"

#Explainy Mayari
    m "You can't be a magic student if you're hungry. You just can't."

#Surprised MC
    mc "You're really passionate about food, huh?"

#Smiley Mayari
    m "More I'm passionate about eating. {w=1}Anyway! I'll show you what's the good stuff to eat."

#Explany Mayari
    m "It's vital to know what's good if you're eating here with someone. They'll want to stay with you longer to get to know you if you have good taste in food."

#Confused MC
    mc "I don't think it works that way."

#Neutral Mayari
    m "Wait and see. Now come on, let's get in line."

    scene bg_studentstore
    with dissolve

#Neutral Mayari
    m "Here's the student store. You can buy all your supplies for classes here plus some snacks."

#Neutral MC
    mc "More food?"

#Indiginate Mayari
    m "What did I tell you? Food is life, oh eff see there's more!"

#Nervous MC
    mc "I didn't say it was a bad thing."

#Tired Mayari
    m "You'll thank me for it later. The snacks here will be the only get you through the day during exams."

#Thinky Mayari
    m "Well, that and caffiene. And some of the supplies here might help with studying too. Not that I'd know."

    scene bg_courtyard
    with dissolve

#Neutral MC
    mc "So come here if I want snacks and maybe some supplies for exams. Got it."

#Neutral Mayari
    m "This is the courtyard, where everyone comes out for some freedom."
    
#Sly MC
    mc "Let me guess, and you can eat out here too, right?"

#Neutral Mayari
    m "Well duh. That's obvious. But it's also good to hang out with some friends or get a bit of alone time with someone special. And the lighting here is just great for selfies."

#Confused MC
    mc "Selfies? On a phone?"

#Confused Mayari
    m "What else would you take a selfie on?"

#Nervous MC
    mc "That's not... it's true but... I wasn't..."

#Smiley Mayari
    m "Oh, but you should be careful who you take your selfies with. Some people may get jelly if you're doing it with the same person too much."
