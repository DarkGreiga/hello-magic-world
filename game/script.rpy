init python:
    ## Default items are marked with the index number 0.
    ##
    ## Current customizations editable by user:
    ## 1. Hairstyles
    ## 2. Hair color
    ##
    ## Other customization that's not editable by user:
    ## 1. Facial expressions
    ## 2. Clothes

    # variable list
    red = -1  #color value for red
    green = -1  #color value for green
    blue = -1  #color value for blue
    is_running = False  #variable to check if the game is currently running
    unlockable_class = []  #list of classes to unlock
    unlockable_activity = []  #list of activities to unlock
    day_name = 0  #selected day, 0 if nothing is selected (schedule)
    time_name = 0  #selected time, 0 if nothing is selected (schedule)
    cur_schedule = []  #the current schedule

    # constant list
    class_list = {
        # list of available classes the player can take
        "class1": Classes("class1", "Class 1", [[1,1],[1,0]], True),
        "class2": Classes("class2", "Class 2", [[2,2],[1,0]], True),
        "class3": Classes("class3", "Class 3", [[3,3]], True),
        "class4": Classes("class4", "Class 4", [[4,3]], False),
        "class5": Classes("class5", "Class 5", [[5,1]], False),
        "class6": Classes("class6", "Class 6", [[6,2]], False),
    }
    activity_list = {
        #list of available activities the player can do
        "activity1": Activities("activity1", "Activity 1", [[1,1],[2,0]], True),
        "activity2": Activities("activity2", "Activity 2", [[2,2]], False),
        "activity3": Activities("activity3", "Activity 3", [[3,3]], False),
        "activity4": Activities("activity4", "Activity 4", [[4,3]], True),
        "activity5": Activities("activity5", "Activity 5", [[5,1]], False),
        "activity6": Activities("activity6", "Activity 6", [[6,2]], False)
    }
    schedule_class_list = [
        # initial available class list
        # dummy
        [
            # morning
            [],
            # noon
            [],
            # evening
            [],
            # night
            []
        ],
        # monday
        [
            # morning
            # ["class1", "class2"],
            [],
            # noon
            # ["class2", "class1"],
            [],
            # evening
            # ["class1"],
            [],
            # night
            # ["class1", "class1"]
            []
        ],
        # tuesday
        [
            # morning
            # ["class3", "class2"],
            [],
            # noon
            # ["class2", "class3"],
            [],
            # evening
            # ["class2"],
            [],
            # night
            # ["class2", "class2", "class5"]
            []
        ],
        # wednesday
        [
            # morning
            # ["class3", "class4"],
            [],
            # noon
            # ["class4", "class3"],
            [],
            # evening
            # ["class3"],
            [],
            # night
            # ["class3", "class3"]
            []
        ],
        # thursday
        [
            # morning
            # ["class4", "class3"],
            [],
            # noon
            # ["class3", "class4"],
            [],
            # evening
            # ["class4"],
            [],
            # night
            # ["class4", "class4"]
            []
        ],
        # friday
        [
            # morning
            # ["class4", "class5"],
            [],
            # noon
            # ["class5", "class4"],
            [],
            # evening
            # ["class5"],
            [],
            # night
            # ["class5", "class5"]
            []
        ],
        # saturday
        [
            # morning
            # ["class5", "class6"],
            [],
            # noon
            # ["class6", "class5"],
            [],
            # evening
            # ["class6"],
            [],
            # night
            # ["class6", "class6"]
            []
        ],
        # sunday
        [
            # morning
            [],
            # noon
            [],
            # evening
            [],
            # night
            []
        ]
    ]
    schedule_activity_list = [
        # initial available activity list
        # dummy
        [
            # morning
            [],
            # noon
            [],
            # evening
            [],
            # night
            []
        ],
        # monday
        [
            # morning
            # ["activity1", "activity2"],
            [],
            # noon
            # ["activity2", "activity1"],
            [],
            # evening
            # ["activity1"],
            [],
            # night
            # ["activity1", "activity1"]
            []
        ],
        # tuesday
        [
            # morning
            # ["activity3", "activity2"],
            [],
            # noon
            # ["activity2", "activity3"],
            [],
            # evening
            # ["activity2"],
            [],
            # night
            # ["activity2", "activity2"]
            []
        ],
        # wednesday
        [
            # morning
            # ["activity3", "activity4"],
            [],
            # noon
            # ["activity4", "activity3"],
            [],
            # evening
            # ["activity3"],
            [],
            # night
            # ["activity3", "activity3"]
            []
        ],
        # thursday
        [
            # morning
            # ["activity4", "activity3"],
            [],
            # noon
            # ["activity3", "activity4"],
            [],
            # evening
            # ["activity4"],
            [],
            # night
            # ["activity4", "activity4"]
            []
        ],
        # friday
        [
            # morning
            # ["activity4", "activity5"],
            [],
            # noon
            # ["activity5", "activity4"],
            [],
            # evening
            # ["activity5"],
            [],
            # night
            # ["activity5", "activity5"]
            []
        ],
        # saturday
        [
            # morning
            # ["activity5", "activity6"],
            [],
            # noon
            # ["activity6", "activity5"],
            [],
            # evening
            # ["activity6"],
            [],
            # night
            # ["activity6", "activity6"]
            []
        ],
        # sunday
        [
            # morning
            [],
            # noon
            [],
            # evening
            [],
            # night
            # ["activity4"]
            []
        ]
    ]

    # header title for schedule (can be written on the image as well)
    header_title = ["Day/Time", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]

    # time title for schedule
    time_title = ["Morning", "Noon", "Evening", "Night"]

    # this function draws the character sprites
    def draw_char(st,at,name):
        return LiveComposite(
        (361,702),                           # 'finished' image size
        (0,0),"base.png",                    # (x,y) position of image, image to display
        (0,0),recolor("{0}hair.png".format(name)),
        (0,0),"{0}shirt.png".format(name),
        (0,0),"{0}pants.png".format(name)
        ),.1

    # this function draws the character portraits
    def draw_portrait(st,at,name):
        return LiveComposite(
        (361,702),
        (10,500),im.Crop("base.png",(120,0,120,204)),
        (10,500),im.Crop(recolor("{0}hair.png".format(name)),(120,0,120,204)),
        (10,500),im.Crop("{0}shirt.png".format(name),(120,0,120,204)),
        (10,500),im.Crop("{0}pants.png".format(name),(120,0,120,204))
        ),.1

    # this function draws the BG with any other overlays
    def draw_bg(st,at):
        return LiveComposite(
        (1280,1024),
        (0,0),"bg.jpg",
        (0,0),"java.png"
        ),.1

    # this function recolors an image
    def recolor(img):
        if [red,green,blue] == [-1,-1,-1]:
            return img
        else:
            return im.MatrixColor(img,im.matrix.desaturate() * im.matrix.tint(red/255.0,green/255.0,blue/255.0))

    # this function unlocks a class or activity. Takes the short name and boolean flag if it's a class
    def unlock_schedule(name, is_class):
        if is_class:
            # remove the class from unlockables
            store.unlockable_class.remove(name)
            store.class_list[name].is_locked = False

            # get the object from the map, get the availability list
            temp = class_list[name].availability

            # for every availability, append the class name to the schedule
            for i in range(len(temp)):
                store.schedule_class_list[temp[i][0]][temp[i][1]].append(name)

        else:
            # remove the activity from unlockables
            store.unlockable_activity.remove(name)
            store.activity_list[name].is_locked = False

            # get the object from the map, get the availability list
            temp = activity_list[name].availability

            # for every availability, append the activity name to the schedule
            for i in range(len(temp)):
                store.schedule_activity_list[temp[i][0]][temp[i][1]].append(name)

    # this function initializes the schedule
    def init_schedule():
        # initialize the classes
        for c in class_list.values():
            if c.is_locked:
                store.unlockable_class.append(c.short_name)
            else:
                temp = c.availability
                for i in range(len(temp)):
                    store.schedule_class_list[temp[i][0]][temp[i][1]].append(c.short_name)

        # initialize the activities
        for a in activity_list.values():
            if a.is_locked:
                store.unlockable_activity.append(a.short_name)
            else:
                temp = a.availability
                for i in range(len(temp)):
                    store.schedule_activity_list[temp[i][0]][temp[i][1]].append(a.short_name)

        # initialize the current schedule
        for i in range(7 * 4):
            store.cur_schedule.append(None)

        return

    # this function clears the player's current schedule setup
    def clear_schedule():
        for i in range(7 * 4):
            store.cur_schedule[i] = None

    # this function adds a schedule item to the current schedule setup
    def add_to_schedule(item):
        i = time_name * 7 + day_name - 1
        store.cur_schedule[i] = item

    init_schedule()

# script to define the characters and their image label
define aeolus = Character("Aeolus",image="aeolus")
define helios = Character("Helios",image="helios")

# script to define the characters' full image
image aeolus img = DynamicDisplayable(draw_char,"aeolus")
image helios img = DynamicDisplayable(draw_char,"helios")

# script to define the characters' side image (if needed)
image side aeolus = DynamicDisplayable(draw_portrait,"aeolus")
image side helios = DynamicDisplayable(draw_portrait,"helios")

# script to define the background images
image bg = DynamicDisplayable(draw_bg)
image bg apartment = "bg_apartment.png"

label start:
    menu:
        "Debugging menu":
            jump debugging_menu
        "Game":
            jump game_start

label game_start:
    python:
        name = renpy.input(_("What is your name?"))

        name = name.strip() or _("MC")

    python:
        surname = renpy.input(_("What is your surname?"))

        surname = surname.strip() or _("Protag")

    $ male_flag = False

    $ female_flag = False

    "Are you male or female?"

    menu:

        "Male":
            $ male_flag = True

        "Female":
            $ female_flag = True

    # call the actual MC maker screen here
    call screen mc_maker with dissolve

label debugging_menu:
    menu:
        "MC maker":
            # call the debug MC maker screen here
            call screen mc_maker_debug

        "Schedule screen":
            call screen schedule_debug

label img_debugging:
    show aeolus img with dissolve

    aeolus "I've dressed myself up."

    hide aeolus img with dissolve

    jump start

label debugging_unlock_classes:
    if len(unlockable_class) == 0:
        "No classes to unlock"
    else:
        python:
            menu_items = []
            choice = None
            for i in unlockable_class:
                menu_items.append((class_list[i].long_name,class_list[i].short_name))
            choice = renpy.display_menu(menu_items)
            unlock_schedule(choice, True)
            unlocked = class_list[choice].long_name
        "[unlocked] is unlocked."

    jump start

label debugging_unlock_activities:
    if len(unlockable_activity) == 0:
        "No activities to unlock"
    else:
        python:
            menu_items = []
            choice = None
            for i in unlockable_activity:
                menu_items.append((activity_list[i].long_name,activity_list[i].short_name))
            choice = renpy.display_menu(menu_items)
            unlock_schedule(choice, False)
            unlocked = activity_list[choice].long_name
        "[unlocked] is unlocked."

    jump start
