style frame_style:
    xalign 0.5
    yalign 0.5

style header_button_style:
    # background "#111"
    # background "schedule_topheader.png"
    xpadding 5
    ypadding 5
    xalign 0.5
    yalign 0.5
    xsize 141
    ysize 54

style schedule_cell:
    xsize 141
    ysize 54

style class_activity:
    xsize 550
    yfill True

style class_activity2:
    xsize 550
    ysize 60

style bottom_right_btn:
    yalign 0.9

style class_activity_text:
    xalign 0.2
    yalign 0.5

style class_activity_text2:
    xalign 0.2
    yalign 0.1
